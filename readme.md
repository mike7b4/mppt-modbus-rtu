# EpEver modbus-rtu reader

Simple tool to read from EpEver MPPT solary charger.
Data is sent via a websocket.

# Dependies

 - tokio-modbus
 - structopt
 - serde
 - tungstenite
 - log

# Todo

 - Add more register
 - More safe writes.

