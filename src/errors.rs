use std::fmt;
pub enum Error {
    Comport(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use crate::errors::Error::*;
        match &self {
            Comport(e) => write!(f, "Communication error: {}", e),
        }
    }
}
