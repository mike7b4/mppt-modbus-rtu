use log::{error, info};
use std::net::{TcpListener, TcpStream};
use tungstenite::{accept, Message, WebSocket};
pub struct WSLogger {
    tcp: TcpListener,
    clients: Vec<WebSocket<TcpStream>>,
}

impl WSLogger {
    pub fn new(listen: &str) -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Self {
            tcp: TcpListener::bind(listen)
                .and_then(|tcp| Ok(tcp.set_nonblocking(true).map(|_| tcp)?))?,
            clients: Vec::new(),
        })
    }

    pub fn incoming(&mut self) -> Result<(), Box<dyn std::error::Error>> {
        for client in self.tcp.incoming() {
            match client {
                Ok(client) => {
                    let socket = accept(client)
                        .map(|socket| {
                            println!(
                                "Client connection from: {}",
                                socket
                                    .get_ref()
                                    .peer_addr()
                                    .map(|a| a.to_string())
                                    .unwrap_or_else(|_| String::from("?"))
                            );
                            Some(socket)
                        })
                        .unwrap_or_else(|e| {
                            error!("Could not handshake Websocket for {}", e);
                            None
                        });

                    if let Some(socket) = socket {
                        self.clients.push(socket);
                    }
                }
                Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => break,
                Err(e) => {
                    return Err(Box::new(e));
                }
            }
        }
        Ok(())
    }

    pub fn num_clients(&self) -> usize {
        self.clients.len()
    }

    // Remove dead clients
    pub fn watchdog(&mut self) {
        // Remove dead clients
        self.clients
            .retain(|client| client.get_ref().peer_addr().is_ok());
    }

    pub fn send_message(&mut self, msg: &str) {
        for client in self.clients.iter_mut() {
            let msg = Message::Text(msg.into());
            if let Err(e) = client.write_message(msg) {
                info!("Removing client: {:?} cause: {}", client, e);
            }
        }
    }
}
