use crate::real_time_status::*;
use std::path::Path;
use tokio_modbus::prelude::*;
use tokio_serial::{Serial, SerialPortSettings};
pub struct EpEver {
    ctx: Box<dyn Reader>,
}

impl EpEver {
    pub async fn new(slave: u8, device: &Path) -> Result<Self, Box<dyn std::error::Error>> {
        let mut settings = SerialPortSettings::default();
        settings.baud_rate = 115200;
        let port = Serial::from_path(device, &settings)?;
        Ok(Self {
            ctx: Box::new(rtu::connect_slave(port, Slave(slave)).await?),
        })
    }

    pub async fn real_time_status(&mut self) -> Result<RealTimeStatus, Box<dyn std::error::Error>> {
        Ok(RealTimeStatus::registers(&mut *self.ctx).await?)
    }
}
