mod epever;
mod real_time_status;
mod wslogger;
use epever::EpEver;
use log::info;
use real_time_status::*;
use std::path::PathBuf;
use structopt::StructOpt;
use wslogger::WSLogger;

#[derive(StructOpt)]
struct Args {
    #[structopt(short, long, default_value = "/dev/ttyUSB0")]
    device: PathBuf,
    #[structopt(short, long, default_value = "1")]
    slave_id: u8,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::builder()
        .filter(None, log::LevelFilter::Info)
        .init();
    use std::sync::{
        atomic::{AtomicBool, Ordering},
        {Arc, Mutex},
    };
    let terminate = Arc::new(AtomicBool::new(false));
    let status = Arc::new(Mutex::new(RealTimeStatus::default()));
    signal_hook::flag::register(libc::SIGINT, terminate.clone()).ok();
    signal_hook::flag::register(libc::SIGTERM, terminate.clone()).ok();
    let args = Args::from_args();

    let mut ep = match EpEver::new(0x01, &args.device).await {
        Ok(ep) => ep,
        Err(e) => {
            log::error!(
                "Open CAN bus device: {} failed cause: {}",
                args.device.display(),
                e
            );
            std::process::exit(1);
        }
    };

    info!(
        "Opened CAN bus at: {} connect to slave ID: 0x{:02X}",
        args.device.display(),
        args.slave_id
    );

    let serv_terminate = terminate.clone();
    let serv_status = status.clone();
    let mut serv = WSLogger::new("127.0.0.1:4040")?;
    std::thread::spawn(move || {
        while !serv_terminate.load(Ordering::Relaxed) {
            let num_clients = serv.num_clients();
            serv.watchdog();
            if serv.num_clients() != num_clients {
                info!("Clients: {}", serv.num_clients());
            }

            if let Err(e) = serv.incoming() {
                log::error!("TCP Server error {}. Terminating...", e);
                serv_terminate.store(true, Ordering::Relaxed);
            }

            match serv_status.lock() {
                Ok(mut st) if st.modified => {
                    st.modified = false;
                    let mut ser = csv::WriterBuilder::new()
                        .has_headers(false)
                        .terminator(csv::Terminator::CRLF)
                        .from_writer(vec![]);
                    ser.serialize(&*st)
                        .map(|_| ser.into_inner().unwrap())
                        .map(|status| serv.send_message(&String::from_utf8_lossy(&status)))
                        .unwrap_or_else(|e| eprintln!("CSV Serialize failed cause: {}", e));
                }
                Ok(_) | Err(_) => {}
            }

            std::thread::sleep(std::time::Duration::from_millis(200));
        }
    });

    let mut ticks = 0;
    while !terminate.load(Ordering::Relaxed) {
        if (ticks % 10) == 0 {
            let st = ep.real_time_status().await?;
            if let Ok(mut status) = status.lock() {
                *status = st;
            }
        }
        ticks += 1;
        std::thread::sleep(std::time::Duration::from_millis(1000));
    }

    info!("Terminated");

    Ok(())
}
