use serde::Serialize;
use std::convert::TryFrom;
use tokio_modbus::prelude::*;

#[derive(Serialize, Clone, Default)]
pub struct RealTimeStatus {
    utc: String,
    solary_voltage: f32,
    solary_current: f32,
    solary_power: f32,
    battery_voltage: f32,
    battery_current: f32,
    battery_power: f32,
    load_voltage: f32,
    load_current: f32,
    load_power: f32,
    #[serde(skip)]
    pub modified: bool,
}

impl TryFrom<&Vec<u16>> for RealTimeStatus {
    type Error = std::io::Error;
    fn try_from(bytes: &Vec<u16>) -> Result<Self, std::io::Error> {
        if bytes.len() != 12 {
            return Err(std::io::Error::new(
                std::io::ErrorKind::InvalidData,
                "Solary status expect 12 bytes",
            ));
        }
        // Wattage is 32 bits (2 words).
        let w: u32 = ((bytes[3] as u32) << 16) as u32 + bytes[2] as u32;
        let solary_power = w as f32 / 100.0;
        let w: u32 = ((bytes[7] as u32) << 16) as u32 + bytes[6] as u32;
        let battery_power = w as f32 / 100.0;
        let w: u32 = ((bytes[11] as u32) << 16) as u32 + bytes[10] as u32;
        let load_power = w as f32 / 100.0;
        let utc = format!("{}", chrono::Utc::now());

        Ok(Self {
            utc,
            solary_voltage: bytes[0] as f32 / 100.0,
            solary_current: bytes[1] as f32 / 100.0,
            solary_power,
            battery_voltage: bytes[4] as f32 / 100.0,
            battery_current: bytes[5] as f32 / 100.0,
            battery_power,
            load_voltage: bytes[8] as f32 / 100.0,
            load_current: bytes[9] as f32 / 100.0,
            load_power,
            modified: true,
        })
    }
}

impl RealTimeStatus {
    pub async fn registers(ctx: &mut dyn Reader) -> Result<Self, std::io::Error> {
        let r = ctx.read_input_registers(0x3100, 12).await?;
        RealTimeStatus::try_from(&r)
    }
}
